package Server;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {

    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newCachedThreadPool();

        LOGGER.info("Server starting...");
        ServerSocket listener = null;
        int clientCount = 0;
        try {
            listener = new ServerSocket(9090);
            while (true) {
//                ClientHandler clientHandler = new ClientHandler(listener.accept(), clientCount++);
//                clientHandler.start();
                Runnable worker = new WorkerThread(listener.accept(), clientCount++);
                executorService.execute(worker);
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error while listening to client connections");
        } finally {
            executorService.shutdown();
            while (!executorService.isTerminated()) {
            }
            IOUtils.closeQuietly(listener);
        }
    }

    private static void first() {
        ServerSocket listener = null;
        try {
            listener = new ServerSocket(9090);
            while (true) {
                Socket socket = listener.accept();
                PrintWriter out = null;
                try {
                    out = new PrintWriter(socket.getOutputStream(), true);
                    out.println(new Date().toString());
                } finally {
                    IOUtils.closeQuietly(out);
                    IOUtils.closeQuietly(socket);
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error while listening to client connections");
        } finally {
            IOUtils.closeQuietly(listener);
        }
    }
}
