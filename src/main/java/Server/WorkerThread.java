package Server;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Sandamal on 9/30/2015.
 */
public class WorkerThread implements Runnable {
    private Socket socket;
    private int clientNumber;
    private static final Logger LOGGER = Logger.getLogger(ClientHandler.class.getName());


    public WorkerThread(Socket socket, int clientNumber) {
        this.socket = socket;
        this.clientNumber = clientNumber;
        LOGGER.info("Client.Client " + clientNumber + "connected on Socket " + socket);
    }


    public void run() {
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

            // Send a welcome message to the client.
            out.println("Hello, your client number is " + clientNumber + ".");
            out.println("Enter a line with only a period to quit\n");

            System.out.println(Thread.currentThread().getName() + " replied to client " + clientNumber);

            while (true) {
                String input = in.readLine();
                if (input == null || ".".equals(input)) {
                    break;
                }
                out.println(input.toUpperCase());
            }

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error handling client " + clientNumber);
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
        }
    }
}
