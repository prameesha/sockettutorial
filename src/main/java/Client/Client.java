package Client;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Sandamal on 9/29/2015.
 */
public class Client {

    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());

    public static void main(String[] args) {

        BufferedReader in = null;
        PrintWriter out = null;
        Scanner scanner = null;
        try {
            Socket socket = new Socket("127.0.0.1", 9090);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

            for (int i = 0; i < 3; i++) {
                LOGGER.info(in.readLine());
            }
            scanner = new Scanner(System.in);
            while (true) {
                String input = scanner.next();
                out.println(input);

                String response = in.readLine();
                if (response == null || response.equals("")) {
                    System.exit(0);
                }
                System.out.println(response);
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error while receiving the message");
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(scanner);
        }
    }

    private static void first() {

        Socket s = null;
        try {
            s = new Socket("127.0.0.1", 9090);
            BufferedReader input =
                    new BufferedReader(new InputStreamReader(s.getInputStream()));
            String answer = input.readLine();
            LOGGER.info(answer);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error while receiving the message");
        } finally {
//            if (s != null) {
//                try {
//                    s.close();
//                } catch (IOException e) {
//                    //ignore
//                }
//            }
            IOUtils.closeQuietly(s);
        }
    }
}
